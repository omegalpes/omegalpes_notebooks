#! usr/bin/env python3
#  -*- coding: utf-8 -*-

from pulp import LpStatus
from omegalpes.energy.energy_nodes import EnergyNode
from omegalpes.energy.units.consumption_units import FixedConsumptionUnit, \
    VariableConsumptionUnit
from omegalpes.energy.units.conversion_units import \
    ElectricalToThermalConversionUnit, HeatPump
from omegalpes.energy.units.production_units import ProductionUnit
from omegalpes.energy.units.storage_units import StorageUnit
from omegalpes.general.optimisation.model import OptimisationModel
from omegalpes.general.time import TimeUnit
from omegalpes.general.utils.plots import plot_quantity, plt, \
    plot_node_energetic_flows

from ipywidgets import widgets, Layout, Button, Box, FloatText, Textarea, \
    Dropdown, Label, IntSlider, FloatSlider, BoundedFloatText, ToggleButton, \
    BoundedIntText, HTML
from IPython.display import clear_output

# ------------------------------------------------------------------------------

# Module version
__version_info__ = (0, 3, 0)
__version__ = ".".join(str(x) for x in __version_info__)

# Documentation strings format
__docformat__ = "restructuredtext en"


# ------------------------------------------------------------------------------

def waste(elec2heat_ratio=0.9, pc_max=5000, pd_max=5000, pc_min=1000,
         pd_min=1000, e_max=20000, cop_hp=3, pmax_elec_hp=1000,
         storage_soc_0=0.2):

    global time, indus, district_heat_load, heat_pump, heat_production, \
        heat_node_bef_valve, heat_node_aft_valve, \
        heat_node_aft_hp, thermal_storage, dissipation, model

    # OPTIMIZATION MODEL
    # Creating the unit dedicated to time management
    time = TimeUnit(periods=24 * 7, dt=1)

    # Creating an empty model
    model = OptimisationModel(time=time, name='waste_e_recovery_model')


    # Importing time-dependent data from files
    indus_cons_file = open("./data/indus_cons_week.txt", "r")
    heat_load_file = open("./data/District_heat_load_consumption.txt", "r")

    # Creating the electro-intensive industry unit
    indus_cons = [c for c in map(float, indus_cons_file)]
    indus = ElectricalToThermalConversionUnit(time, 'indus',
                                              elec_to_therm_ratio=elec2heat_ratio,
                                              p_in_elec=indus_cons)

    # Creating unit for heat dissipation from the industrial process
    dissipation = VariableConsumptionUnit(time, 'dissipation',
                                          energy_type='Thermal')

    # Creating the thermal storage
    thermal_storage = StorageUnit(time, 'thermal_storage', pc_max=pc_max,
                                  pd_max=pd_max, pc_min=pc_min, pd_min=pd_min,
                                  capacity=e_max, e_0=storage_soc_0 * e_max)

    # Creating the heat pump
    heat_pump = HeatPump(time, 'heat_pump', cop=cop_hp,
                         pmax_in_elec=pmax_elec_hp)

    # Creating the district heat load
    heat_load = [c for c in map(float, heat_load_file)]
    district_heat_load = FixedConsumptionUnit(time, 'district_heat_load',
                                              p=heat_load,
                                              energy_type='Thermal')

    # Creating the heat production plants
    heat_production = ProductionUnit(time, name='heat_production',
                                     energy_type='Thermal')

    # Creating the heat node for the energy flows
    heat_node_bef_valve = EnergyNode(time, 'heat_node_bef_valve',
                                     energy_type='Thermal')
    heat_node_aft_valve = EnergyNode(time, 'heat_node_aft_valve',
                                     energy_type='Thermal')
    heat_node_aft_hp = EnergyNode(time, 'heat_node_aft_hp',
                                  energy_type='Thermal')

    # Connecting units to the nodes
    heat_node_bef_valve.connect_units(indus.thermal_production_unit,
                                      dissipation)
    heat_node_bef_valve.export_to_node(
        heat_node_aft_valve)  # Export after the valve
    heat_node_aft_valve.connect_units(thermal_storage,
                                      heat_pump.thermal_consumption_unit)

    heat_node_aft_hp.connect_units(heat_pump.thermal_production_unit,
                                   heat_production, district_heat_load)

    # OBJECTIVE CREATION
    # Minimizing the part of the heat load covered by the heat production plant
    heat_production.minimize_production()

    # Adding all nodes (and connected units) to the optimization model
    model.add_nodes(heat_node_bef_valve, heat_node_aft_valve, heat_node_aft_hp)

    # model.writeLP('waste_e_recovery.lp')   # Writing into lp file
    model.solve_and_update()  # Running optimization and update values


def waste_results():
    """
        *** This function prints the optimisation result:
                - The district consumption during the year
                - The industry consumption during the year
                - The district heat network production during the year
                - The heat exported from the industry
                - The rate of the load covered by the industry

            And plots the power curves :
            On the first figure : the energy out of the industry with the
            recovered and the dissipated parts
            On the second figure: the energy on the district heating network
            with the part produced by the heat pump and
            the part produced by the district heating production unit.

    """

    # Print results
    if LpStatus[model.status] == 'Optimal':
        print("\n - - - - - OPTIMIZATION RESULTS - - - - - ")
        print('District consumption = {0} MWh.'.format(
            round(district_heat_load.e_tot.value / 1e3, 2)))
        print('Industrial electrical consumption = {0} MWh'.format(
              round(indus.elec_consumption_unit.e_tot.value / 1e3, 2)))
        print('District heating network production  production = {0} MWh'.format(
              round(heat_production.e_tot.value / 1e3, 2)))   
        print('Industry heat exported = {0} MWh.'.format(
            round(sum(
                heat_node_bef_valve.energy_export_to_heat_node_aft_valve
                    .value.values())/1e3)))
        print('Heat pump electricity consumption = {0} MWh'.format(
              round(heat_pump.elec_consumption_unit.e_tot.value / 1e3, 2)))
        print("{0} % of the load coming from the industry".format(
            round(sum(list(
                heat_node_bef_valve.energy_export_to_heat_node_aft_valve
                    .value.values())) /
                  district_heat_load.e_tot.value * 100)))  # value is a dict,
        # with time as a key, and power levels as values.

        # SHOW THE GRAPH
        # Recovered and dissipated heat
        print("Recovered and dissipated heat")
        plot_node_energetic_flows(heat_node_bef_valve)

        # Energy on the recovery system
        print("Energy on the recovery system")
        plot_node_energetic_flows(heat_node_aft_valve)

        # Energy on the district heating network
        print("Energy on the district heating network")
        plot_node_energetic_flows(heat_node_aft_hp)

        plt.show()

    elif LpStatus[model.status] == 'Infeasible':
        print("Sorry, the optimisation problem has no feasible solution !")

    elif LpStatus[model.status] == 'Unbounded':
        print("The cost function of the optimisation problem is unbounded !")

    elif LpStatus[model.status] == 'Undefined':
        print("Sorry, a feasible solution has not been found (but may exist). "
              "PuLP does not manage to interpret the solver's output, "
              "the infeasibility of the MILP problem may have been "
              "detected during presolve")

    else:
        print("Sorry, the optimisation problem has not been solved.")


    
def dashboard():
    def runplot(click):
        clear_output()
        waste_results()
        display(form)
        form.children[len(form_items)-1].on_click(updateData)


    def updateData(click):
        clear_output()
        i=1;
        ELEC_TO_HEAT_RATIO = form.children[i].children[1].value/100; i+=1
        PC_MAX_STORAGE = PD_MAX_STORAGE = form.children[i].children[1].value; i+=1
        PC_MIN_STORAGE = PD_MIN_STORAGE = form.children[i].children[1].value*PC_MAX_STORAGE/100; i+=1
        CAPA_STORAGE = form.children[i].children[1].value; i+=1  
        # Storage capacity of 20MWh
        SOC_0_STORAGE = form.children[i].children[1].value/100; i+=1    # Initial state of charge of 25%
        COP = form.children[i].children[1].value; i+=1            # The coefficient of performance equals 3
        P_MAX_HP = form.children[i].children[1].value; i+=1    # The heat pump has a electrical power limit of 1 MW
        waste(elec2heat_ratio=ELEC_TO_HEAT_RATIO, pc_max=PC_MAX_STORAGE,
             pd_max=PD_MAX_STORAGE, pc_min=PC_MIN_STORAGE,
             pd_min=PD_MIN_STORAGE, e_max=CAPA_STORAGE, cop_hp=COP,
             pmax_elec_hp=P_MAX_HP, storage_soc_0=SOC_0_STORAGE)
        display(runPlotButton)
        runPlotButton.on_click(runplot)
    
    
    form_item_layout = Layout(
        display='flex',
        flex_flow='row',
        justify_content='space-between'
    )


    
    form_items = [
        Box([HTML(value="Here you update the inputs. Then, press <b>Update</b> to run the optimisation. If the there is a solution you will be able to <b>Plot results</b>. In case of no sulition, it will show a warning.",placeholder='',description='')],  layout=Layout(width='auto', grid_area='header')),
        Box([Label(value='Electrical-heat convertion ratio (%)'), IntSlider(min=0, max=100, step=1,value=85)], layout=form_item_layout),
        Box([Label(value='Maximal charging and discharging powers (kW)'),
             BoundedIntText(min=1000, max=100000, step=100,value = 6700)], layout=form_item_layout,width='1'),
        Box([Label(value='Minimal charging and discharging powers (% of max)'), IntSlider(min=0, max=20, step=1,value=10)], layout=form_item_layout),
        Box([Label(value='Storage capacity  (kWh)'),
             BoundedIntText(min=1000, max=100000, step=1000,value = 20000)], layout=form_item_layout,width='1'),
        Box([Label(value='Initial state of charge (%)'), IntSlider(min=0, max=100, step=1,value=20)], layout=form_item_layout),
        Box([Label(value='The coefficient of performance'), BoundedIntText(min=0, max=10, step=1,value=3)], layout=form_item_layout),
        Box([Label(value='Pump electrical power limit  (kW)'),
             BoundedIntText(min=0, max=10000, step=10,value = 1260)], layout=form_item_layout,width='1'),
       Button(
        description='Update',
        disabled=False,
        button_style='info', # 'success', 'info', 'warning', 'danger' or ''
        tooltip='Click me',
        icon='check')

    ]

    form = Box(form_items, layout=Layout(
        display='flex',
        flex_flow='column',
        border='solid 2px',
      #  align_items='stretch',
        width='70%'
    ))

    runPlotButton = widgets.Button(
        description='Plot',
        disabled=False,
        button_style='success', # 'success', 'info', 'warning', 'danger' or ''
        tooltip='Click me',
        icon='check')
    
    display(form)
    form.children[len(form_items)-1].on_click(updateData)
    
