#! usr/bin/env python3
#  -*- coding: utf-8 -*-

# imports for oniri
import os
from pulp import LpStatus
from omegalpes.energy.energy_nodes import EnergyNode
from omegalpes.energy.units.consumption_units import FixedConsumptionUnit, \
    VariableConsumptionUnit
from omegalpes.energy.units.production_units import FixedProductionUnit, \
    VariableProductionUnit
from omegalpes.energy.units.storage_units import StorageUnit
from omegalpes.energy.units.conversion_units import \
    ElectricalConversionUnit, ReversibleConversionUnit
from omegalpes.energy.units.reversible_units import ReversibleUnit
from omegalpes.general.optimisation.model import OptimisationModel
from omegalpes.general.utils.plots import plt, plot_quantity_bar, \
    plot_node_energetic_flows
from omegalpes.general.utils.output_data import save_energy_flows
from omegalpes.general.utils.input_data import resample_data
from omegalpes.general.time import TimeUnit
from ipywidgets import widgets, Layout, Button, Box, FloatText, Textarea, \
    Dropdown, Label, IntSlider, FloatSlider, FloatRangeSlider,\
    BoundedFloatText,  ToggleButton, BoundedIntText, HTML, Checkbox
from IPython.display import clear_output

# imports for optimization of the production

from omegalpes.general.utils.plots import plot_quantity, plt


def oniri(work_path, p_alex, p_ez_ju_sound, h_balance, h_show,
          mob, h_dep, h_arr, p_mob, pv_profile, pc_pv, storage_pmax,
          storage_capa, show, balance, l_placing, l_balance, l_show,
          pmax_bicy_prod_no_show, pmax_bicy_prod_show, nb_pdt, dt, h_pv_beg,
          h_pv_end, e0_eq_ef):
    global time, model, battery1, battery2, node_pv1, node_bicycle1, \
        node_video, node_sound_light

    # Adapting values depending on the dt
    nb_pdt = int(nb_pdt / dt)
    h_dep = int(h_dep / dt)
    h_balance = int(h_balance / dt)
    h_arr = int(h_arr / dt)
    l_placing = int(l_placing / dt)
    l_balance = int(l_balance / dt)
    l_show = int(l_show / dt)
    h_pv_beg = int(h_pv_beg / dt)
    h_pv_end = int(h_pv_end / dt)

    if isinstance(h_show, list):
        h_show = [int(hs / dt) for hs in h_show]
    else:
        h_show = int(h_show / dt)

    # Creating an empty model
    time = TimeUnit(periods=nb_pdt, dt=dt)
    # = 1h
    model = OptimisationModel(name='scenario_oniri_new', time=time)

    # Data management
    pv1_profile = [p * pc_pv / 2 for p in resample_data(input_list=pv_profile,
                                                        dt_final=dt)]
    pv2_profile = [p * pc_pv / 2 for p in resample_data(input_list=pv_profile,
                                                        dt_final=dt)]
    # PV use
    for h in [*range(0, h_pv_beg), *range(h_pv_end, nb_pdt)]:
        pv1_profile[h] = 0
        pv2_profile[h] = 0

    # Maximal power for the production of energy with the bicycles out of OR
    # during the the show:
    pmax_bicy_prod = [pmax_bicy_prod_no_show] * nb_pdt

    # limitation of bicycle production during the show and placing
    if isinstance(h_show, list):
        for hs in h_show:
            for h in range(hs - l_placing, hs + l_show + 1):
                pmax_bicy_prod[h] = pmax_bicy_prod_show
    else:
        pmax_bicy_prod[h_show] = pmax_bicy_prod_show

    #
    cons_mob_profile = [0] * nb_pdt
    # if mob:
    #     for h in range(h_dep, h_arr):
    #         cons_mob_profile[h] = p_mob
    #     # No PV production before being onsite.
    #     for h in range(0, h_arr):
    #         pv1_profile[h] = 0
    #         pv2_profile[h] = 0
    #         pv3_profile[h] = 0
    #         pv4_profile[h] = 0

    # PRODUCTION UNITS
    # pv panels
    pv1 = FixedProductionUnit(time, name='pv1', p=pv1_profile)
    pv2 = FixedProductionUnit(time, name='pv2', p=pv2_profile)

    # bicycle generators / mobility
    bicycle1 = ReversibleUnit(time=time, name='bicycle1',
                              p_cons=cons_mob_profile,
                              pmax_prod=pmax_bicy_prod,
                              energy_type_cons='Electrical',
                              energy_type_prod='Electrical')
    bicycle2 = ReversibleUnit(time=time, name='bicycle2',
                              p_cons=cons_mob_profile,
                              pmax_prod=pmax_bicy_prod,
                              energy_type_cons='Electrical',
                              energy_type_prod='Electrical')
    bicycle3 = ReversibleUnit(time=time, name='bicycle3',
                              p_cons=cons_mob_profile,
                              pmax_prod=pmax_bicy_prod,
                              energy_type_cons='Electrical',
                              energy_type_prod='Electrical')
    bicycle4 = ReversibleUnit(time=time, name='bicycle4',
                              p_cons=cons_mob_profile,
                              pmax_prod=pmax_bicy_prod,
                              energy_type_cons='Electrical',
                              energy_type_prod='Electrical')

    # Bicycle can only produce during the day
    h_bicy_prod = [0, int(h_arr * dt)]
    bicycle1.production_unit.add_operating_time_range([h_bicy_prod])
    bicycle2.production_unit.add_operating_time_range([h_bicy_prod])
    bicycle3.production_unit.add_operating_time_range([h_bicy_prod])
    bicycle4.production_unit.add_operating_time_range([h_bicy_prod])

    # Minimizing the production coming from the bicycles
    bicycle1.production_unit.minimize_production()
    bicycle2.production_unit.minimize_production()
    bicycle3.production_unit.minimize_production()
    bicycle4.production_unit.minimize_production()

    # Power supply from the power grid after the show
    power_supply1 = VariableProductionUnit(time=time, name='power_supply1',
                                           p_max=0.15,
                                           energy_type='Electrical')
    power_supply2 = VariableProductionUnit(time=time, name='power_supply2',
                                           p_max=0.15,
                                           energy_type='Electrical')
    power_supply1.add_operating_time_range([[0, int(h_show[0] * dt)],
                                            [int(h_show[-1] * dt) + 2,
                                             23]])
    power_supply2.add_operating_time_range([[0, int(h_show[0] * dt)],
                                            [int(h_show[-1] * dt) + 2,
                                             23]])
    # Minimizing the production coming from power grid
    power_supply1.minimize_production()
    power_supply2.minimize_production()

    # CONSUMPTION
    # Consumption profiles creation
    cons_profile_alex = [0] * nb_pdt
    cons_profile_ez_ju_sound = [0] * nb_pdt
    if balance:
        for lb in range(0, int(l_balance)):
            # length of the balance
            cons_profile_alex[h_balance + lb] = p_alex
            cons_profile_ez_ju_sound[h_balance + lb] = p_ez_ju_sound
    if show:
        for hs in h_show:
            # for each placing
            for lp in range(0, int(l_placing)):
                # length of the placing
                cons_profile_alex[hs - l_placing + lp] = p_alex
            # for each show
            for ls in range(0, int(l_show)):
                # length of the show
                cons_profile_alex[hs + ls] = p_alex
                cons_profile_ez_ju_sound[hs + ls] = p_ez_ju_sound

    # Consumption units
    cons_alex = FixedConsumptionUnit(time, 'cons_alex', p=cons_profile_alex,
                                     energy_type='Electrical')

    cons_ez_ju_sound = FixedConsumptionUnit(time, 'cons_ez_ju_sound',
                                            p=cons_profile_ez_ju_sound,
                                            energy_type='Electrical')
    # In order for the problem to remain feasible when to much consumption.
    cons_excess1 = VariableConsumptionUnit(time, 'cons_exc1')
    cons_excess2 = VariableConsumptionUnit(time, 'cons_exc2')

    # STORAGE
    hourly_self_disch = 0.01 / 24  # 1% per day

    if e0_eq_ef:
        battery1 = StorageUnit(time, name='battery1',
                               pc_max=storage_pmax, pd_max=storage_pmax,
                               eff_c=0.88, eff_d=0.88, soc_min=0.2, soc_max=1,
                               self_disch_t=hourly_self_disch, e_0=1,
                               ef_is_e0=True, capacity=storage_capa)

        battery2 = StorageUnit(time, name='battery2',
                               pc_max=storage_pmax, pd_max=storage_pmax,
                               eff_c=0.88, eff_d=0.88, soc_min=0.2, soc_max=1,
                               self_disch_t=hourly_self_disch, e_0=1,
                               ef_is_e0=True, capacity=storage_capa)
    else:
        battery1 = StorageUnit(time, name='battery1',
                               pc_max=storage_pmax, pd_max=storage_pmax,
                               eff_c=0.88, eff_d=0.88, soc_min=0.2, soc_max=1,
                               self_disch_t=hourly_self_disch, e_0=1,
                               ef_is_e0=False, capacity=storage_capa)

        battery2 = StorageUnit(time, name='battery2',
                               pc_max=storage_pmax, pd_max=storage_pmax,
                               eff_c=0.88, eff_d=0.88, soc_min=0.2, soc_max=1,
                               self_disch_t=hourly_self_disch, e_0=1,
                               ef_is_e0=False, capacity=storage_capa)

    # CONVERSION UNITS
    # mppt
    # Losses are taken into account in the PV profile calculation
    mppt1 = ElectricalConversionUnit(time=time, name='mppt1',
                                     elec_to_elec_ratio=1)
    mppt2 = ElectricalConversionUnit(time=time, name='mppt2',
                                     elec_to_elec_ratio=1)

    # rectifiers
    rec_eff = 0.88
    rectifier1 = ReversibleConversionUnit(time=time, name='rectifier1',
                                          up2down_eff=rec_eff,
                                          down2up_eff=rec_eff,
                                          energy_type_up='Electrical',
                                          energy_type_down='Electrical')
    rectifier2 = ReversibleConversionUnit(time=time, name='rectifier2',
                                          up2down_eff=rec_eff,
                                          down2up_eff=rec_eff,
                                          energy_type_up='Electrical',
                                          energy_type_down='Electrical')
    rectifier3 = ReversibleConversionUnit(time=time, name='rectifier3',
                                          up2down_eff=rec_eff,
                                          down2up_eff=rec_eff,
                                          energy_type_up='Electrical',
                                          energy_type_down='Electrical')
    rectifier4 = ReversibleConversionUnit(time=time, name='rectifier4',
                                          up2down_eff=rec_eff,
                                          down2up_eff=rec_eff,
                                          energy_type_up='Electrical',
                                          energy_type_down='Electrical')

    # inverters
    inverter_alex = ElectricalConversionUnit(time=time,
                                             name='inverter_alex',
                                             elec_to_elec_ratio=0.91)
    inverter_ez_ju_sound = ElectricalConversionUnit(time=time,
                                                    name=
                                                    'inverter_ez_ju_sound',
                                                    elec_to_elec_ratio=0.91)

    # ENERGY NODES
    node_pv1 = EnergyNode(time, 'node_pv1', 'Electrical')
    node_pv2 = EnergyNode(time, 'node_pv2', 'Electrical')

    node_bicycle1 = EnergyNode(time, 'node_bicycle1', 'Electrical')
    node_bicycle2 = EnergyNode(time, 'node_bicycle2', 'Electrical')
    node_bicycle3 = EnergyNode(time, 'node_bicycle3', 'Electrical')
    node_bicycle4 = EnergyNode(time, 'node_bicycle4', 'Electrical')

    node_video = EnergyNode(time, 'node_video', 'Electrical')
    node_sound_light = EnergyNode(time, 'node_sound_light', 'Electrical')

    node_alex = EnergyNode(time, 'node_alex', 'Electrical')
    node_ez_ju_sound = EnergyNode(time, 'node_ez_ju_sound', 'Electrical')

    node_pv1.connect_units(pv1, mppt1.elec_consumption_unit, cons_excess1)
    node_pv2.connect_units(pv2, mppt2.elec_consumption_unit, cons_excess2)
    node_bicycle1.connect_units(bicycle1.production_unit,
                                bicycle1.consumption_unit,
                                rectifier1.rev_unit_upstream.consumption_unit,
                                rectifier1.rev_unit_upstream.production_unit)
    node_bicycle2.connect_units(bicycle2.production_unit,
                                bicycle2.consumption_unit,
                                rectifier2.rev_unit_upstream.consumption_unit,
                                rectifier2.rev_unit_upstream.production_unit)
    node_bicycle3.connect_units(bicycle3.production_unit,
                                bicycle3.consumption_unit,
                                rectifier3.rev_unit_upstream.consumption_unit,
                                rectifier3.rev_unit_upstream.production_unit)
    node_bicycle4.connect_units(bicycle4.production_unit,
                                bicycle4.consumption_unit,
                                rectifier4.rev_unit_upstream.consumption_unit,
                                rectifier4.rev_unit_upstream.production_unit)

    node_video.connect_units(
        rectifier1.rev_unit_downstream.consumption_unit,
        rectifier1.rev_unit_downstream.production_unit,
        rectifier2.rev_unit_downstream.consumption_unit,
        rectifier2.rev_unit_downstream.production_unit,
        mppt1.elec_production_unit,
        inverter_alex.elec_consumption_unit,
        battery1, power_supply1)

    node_sound_light.connect_units(mppt2.elec_production_unit,
                                   rectifier3.rev_unit_downstream.consumption_unit,
                                   rectifier3.rev_unit_downstream.production_unit,
                                   rectifier4.rev_unit_downstream.consumption_unit,
                                   rectifier4.rev_unit_downstream.production_unit,
                                   inverter_ez_ju_sound.elec_consumption_unit,
                                   battery2, power_supply2)

    node_alex.connect_units(inverter_alex.elec_production_unit, cons_alex)
    node_ez_ju_sound.connect_units(inverter_ez_ju_sound.elec_production_unit,
                                   cons_ez_ju_sound)

    model.add_nodes(node_pv1, node_pv2, node_bicycle1,
                    node_bicycle2, node_bicycle3, node_bicycle4,
                    node_video, node_sound_light, node_alex, node_ez_ju_sound)

    # model.writeLP(work_path + r'\optim_models\oniri_new.lp')
    model.solve_and_update()

    return model, time, pv1, pv2, power_supply1, power_supply2, mppt1, mppt2, \
           bicycle1, bicycle2, bicycle3, bicycle4, battery1, battery2, \
           rectifier1, rectifier2, rectifier3, rectifier4, inverter_alex, \
           inverter_ez_ju_sound, node_pv1, node_pv2, node_bicycle1, \
           node_bicycle2, node_bicycle3, node_bicycle4, node_video, \
           node_sound_light, node_alex, node_ez_ju_sound


def print_results_oniri():
    """
        *** This function print the optimisation result
    """

    if LpStatus[model.status] == 'Optimal':
        print("\n - - - - - OPTIMISATION RESULTS - - - - - ")
        plot_quantity_bar(time=time, quantity=battery1.e,
                          title='État de charge de la batterie 1 sur la '
                                'période d\'étude')
        plot_quantity_bar(time=time, quantity=battery2.e,
                          title='État de charge de la batterie 1 sur la '
                                'période d\'étude')
        plot_node_energetic_flows(node_pv1)
        plot_node_energetic_flows(node_bicycle1)
        plot_node_energetic_flows(node_video)
        plot_node_energetic_flows(node_sound_light)
        plt.show()

    elif LpStatus[model.status] == 'Infeasible':
        print("Sorry, the optimisation problem has no feasible solution !")

    elif LpStatus[model.status] == 'Unbounded':
        print("The cost function of the optimisation problem is unbounded !")

    elif LpStatus[model.status] == 'Undefined':
        print("Sorry, a feasible solution has not been found (but may exist). "
              "PuLP does not manage to interpret the solver's output, "
              "the infeasibility of the MILP problem may have been "
              "detected during presolve.")
    else:
        print("Sorry, the optimisation problem has not been solved.")


def dashboard(oniri, print_results_oniri):
    def runplot(click):
        clear_output()
        print_results_oniri()
        display(form)
        form.children[len(form_items) - 1].on_click(updateData)

    def updateData(click):
        clear_output()
        i = 1
        DT = 1 / form.children[i].children[1].value
        i += 1
        P_ALEX = form.children[i].children[1].value
        i += 1
        P_OTHER = form.children[i].children[1].value
        i += 1
        BAL = form.children[i].children[1].value
        i += 1
        H_BAL = form.children[i].children[1].value
        i += 1
        H_SHOW_str = form.children[i].children[1].value.split(",")
        H_SHOW = [float(h) for h in H_SHOW_str]
        i += 1
        L_BAL = form.children[i].children[1].value
        i += 1
        L_PLAC = form.children[i].children[1].value
        i += 1
        L_SHOW = form.children[i].children[1].value
        i += 1
        PV_PROFILE_str = form.children[i].children[1].value.split(",")
        PV_PROFILE = [float(p) for p in PV_PROFILE_str]
        i += 1
        PC_PV = form.children[i].children[1].value
        i += 1
        H_BEG_PV = form.children[i].children[1].value[0]
        H_END_PV = form.children[i].children[1].value[1]
        i += 1
        PMAX_SHOW = form.children[i].children[1].value
        i += 1
        PMAX_NO_SHOW = form.children[i].children[1].value
        i += 1
        CAPA_STO = form.children[i].children[1].value
        i += 1
        E0_EQ_EF = form.children[i].children[1].value
        i += 1

        oniri(work_path=os.getcwd(), p_alex=P_ALEX, p_ez_ju_sound=P_OTHER,
              h_balance=H_BAL, h_show=H_SHOW, mob=False, h_dep=8, h_arr=10,
              p_mob=0.4, pv_profile=PV_PROFILE, pc_pv=PC_PV, storage_pmax=1,
              storage_capa=CAPA_STO, show=True, balance=BAL, l_placing=L_PLAC,
              l_balance=L_BAL, l_show=L_SHOW,
              pmax_bicy_prod_no_show=PMAX_NO_SHOW,
              pmax_bicy_prod_show=PMAX_SHOW, nb_pdt=24, dt=DT,
              h_pv_beg=H_BEG_PV, h_pv_end=H_END_PV, e0_eq_ef=E0_EQ_EF)

        display(runPlotButton)
        runPlotButton.on_click(runplot)

    form_item_layout = Layout(display='flex', flex_flow='row',
                              justify_content='space-between')

    form_items = [
        Box([HTML(value=('Vous pouvez ici entrer et mettre à jour les '
                         'paramètres. Appuyez sur <b>Lancer - Mettre à '
                         'jour</b> pour lancer '
                         'une optimisation.\ Si une solution existe, '
                         'vous pouvez afficher les résultats en cliquant sur '
                         '<b>Plot results</b>.\ Sinon un warning sera '
                         'affiché.'),
                  placeholder='', description='Instruction')],
            layout=Layout(width='auto', grid_area='header')),
        Box([Label(value='Pas de temps (1 = 1h ; 1/2 = 30min ; 1/12 = 5min ; '
                         '...'),
             BoundedIntText(value=12, min=1, max=12, step=1,
                            description='1/', disabled=False)],
            layout=form_item_layout),
        Box([Label(value='Puissance de consommation vidéo'),
             FloatSlider(value=0.31, min=0, max=1.0, step=0.01,
                         description='(kW)', disabled=False)],
            layout=form_item_layout),
        Box([Label(value='Puissance de consommation lumière - son - '
                         'auxiliaires'),
             FloatSlider(value=0.29, min=0, max=1.0, step=0.01,
                         description='(kW)', disabled=False)],
            layout=form_item_layout),
        Box([Label(value='Balance en amont du spectacle ?'),
             Checkbox(value=False, description='', disabled=False)],
            layout=form_item_layout),
        Box([Label(value='Heure de balance'),
             FloatSlider(value=18, min=0.0, max=24.0, step=1 / 12,
                         description='(h)', disabled=False)],
            layout=form_item_layout),
        Box([Label(value='Heure(s) de spectacle'),
             Textarea(placeholder='spectacle 1, specatcle 2, ...',
                      value='9.5, 14.25, 16', description='(h)',
                      layout=Layout(width='40%', height='100px'))],
            layout=form_item_layout),
        Box([Label(value='Durée balance'),
             FloatSlider(value=1 / 4, min=0.0, max=2.0, step=1 / 12,
                              description='(h)', disabled=False)],
            layout=form_item_layout),
        Box([Label(value='Durée mise'),
             FloatSlider(value=1 / 4, min=0.0, max=2.0, step=1 / 12,
                              description='(h)', disabled=False)],
            layout=form_item_layout),
        Box([Label(value='Durée spectacle'),
             FloatSlider(value=1 / 3, min=0.0, max=2.0, step=1 / 12,
                              description='(h)', disabled=False)],
            layout=form_item_layout),
        Box([Label(value='Profil PV horaire pour 1 kW sur la durée d\'étude. '
                         'Les valeurs sont séparées par des virgules et le '
                         'séparateur décimal est un point'),
             Textarea(placeholder='00h, 01h, 02h, 03h, 04h, ..., 23h',
                      value='0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.00435,'
                            '0.01865, 0.12174, 0.17084, 0.09912, 0.06432, '
                            '0.098, 0.01636, 0.00835, 0.00483, 0.0, 0.0, 0.0,'
                            ' 0.0, 0.0, 0.0, 0.0',
                      layout=Layout(width='40%', height='100px'))],
            layout=form_item_layout),
        Box([Label(value='Puissance crète PV'),
             FloatSlider(value=0.440, min=0, max=1.1, step=0.055,
                         description='(kW)', disabled=False)],
            layout=form_item_layout),
        Box([Label(value='Heures de début & fin d\'utilisation PV'),
             FloatRangeSlider(value=[10.5, 13.5], min=0.0, max=24.0, step=1 /2,
                              description='(h)', disabled=False)],
            layout=form_item_layout),
        Box([Label(value='Puissance maximale de production d\'un vélow en '
                         'dehors du spectacle - des mises'),
             FloatSlider(value=0.1, min=0, max=0.2, step=0.05,
                         description='(kW)', disabled=False)],
            layout=form_item_layout),
        Box([Label(value='Puissance maximale de production d\'un vélow '
                         'pendant un spectacle - une mise'),
             FloatSlider(value=0.05, min=0, max=0.2, step=0.05,
                         description='(kW)', disabled=False)],
            layout=form_item_layout),
        Box([Label(value='Capacité d\'une batterie'),
             FloatSlider(value=1, min=0, max=2, step=0.05,
                         description='(kWh)', disabled=False)],
            layout=form_item_layout),
        Box([Label(value='Batteries: état de charge initial = état de charge '
                         'final ?'),
             Checkbox(value=True, description='', disabled=False)],
            layout=form_item_layout),
        Button(
            description='Lancer - Mettre à jour',
            disabled=False,
            button_style='info',
            # 'success', 'info', 'warning', 'danger' or ''
            tooltip='Click me',
            icon='check')
    ]

    form = Box(form_items, layout=Layout(
        display='flex',
        flex_flow='column',
        border='solid 2px',
        width='100%'
    ))

    runPlotButton = widgets.Button(
        description='Plot',
        disabled=False,
        button_style='success',  # 'success', 'info', 'warning', 'danger' or ''
        tooltip='Click me',
        icon='check')

    display(form)
    # for i in [5, 6, 7, 8, 9, 12, 13]:
    #     # All time related parameters step updated with the dt value
    #     jslink((1/form.children[1].children[1], 'value'), (form.children[
    #         i].children[1], 'step'))

    form.children[len(form_items) - 1].on_click(updateData)


# def update_range(form):
#     step = 1 / form.children[1].children[1].value
#     return step
