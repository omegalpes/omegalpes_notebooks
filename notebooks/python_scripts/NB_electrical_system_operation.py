#! usr/bin/env python3
#  -*- coding: utf-8 -*-

from pulp import LpStatus
from omegalpes.energy.energy_nodes import EnergyNode
from omegalpes.energy.units.production_units import VariableProductionUnit
from omegalpes.energy.units.consumption_units import FixedConsumptionUnit
from omegalpes.general.optimisation.model import OptimisationModel
from omegalpes.general.time import TimeUnit
from omegalpes.general.utils.plots import plot_quantity, plt
from ipywidgets import widgets, Layout, Button, Box, FloatText, Textarea, \
    Dropdown, Label, IntSlider, FloatSlider, BoundedFloatText, ToggleButton, \
    BoundedIntText, HTML
from IPython.display import clear_output

# ------------------------------------------------------------------------------

# Module version
__version_info__ = (0, 3, 0)
__version__ = ".".join(str(x) for x in __version_info__)

# Documentation strings format
__docformat__ = "restructuredtext en"


# ------------------------------------------------------------------------------

def production_optim(op_cost_a, op_cost_b, consumption_file):
    global time, model, dwelling_consumption, grid_production_A, \
        grid_production_B
    """
    :param op_cost_a: operating costs for the energy unit A [€]
    :type op_cost_a: list of 24 float or int
    :param op_cost_b: operating costs for the energy unit B [€]
    :type op_cost_b: list of 24 float or int
    :param consumption_file: link to the data file
    :type consumption_file: string

    """
    # OPTIMIZATION MODEL
    # Creating the unit dedicated to time management
    time = TimeUnit(periods=24, dt=1)

    # Creating an empty model
    model = OptimisationModel(time=time, name='elec_prod_simple_example')

    # Creating the unit dedicated to time management, and importing
    # time-dependent data
    building_cons_file = open(consumption_file, "r")

    # Creating the dwelling consumption - the load profile is known
    electrical_load = [c for c in map(float, building_cons_file)]
    dwelling_consumption = FixedConsumptionUnit(time, 'dwelling_consumption',
                                                energy_type='Electrical',
                                                p=electrical_load)

    # Creating the production units - the production profile are unknown
    grid_production_A = VariableProductionUnit(time=time,
                                               name='grid_production_A',
                                               energy_type='Electrical',
                                               operating_cost=op_cost_a)

    grid_production_B = VariableProductionUnit(time=time,
                                               name='grid_production_B',
                                               energy_type='Electrical',
                                               operating_cost=op_cost_b)

    # Objective : Minimizing the part of the electrical load covered by the
    # electrical production plants
    grid_production_A.minimize_operating_cost()
    grid_production_B.minimize_operating_cost()

    # Creating the energy nodes and connecting units
    elec_node = EnergyNode(time, 'elec_node', energy_type='Electrical')
    elec_node.connect_units(dwelling_consumption, grid_production_A,
                            grid_production_B)

    # Adding the energy node to the model
    model.add_nodes(elec_node)

    # Optimisation process
    # model.writeLP('elec_prod_simple_example.lp')
    model.solve_and_update()  # Run optimization and update values

    return model, time, dwelling_consumption, grid_production_A, \
           grid_production_B


def print_results_optimization():
    """
        *** This function prints the optimisation result:
                - The dwelling consumption
                - The grid_production A
                - The grid_production B

            Then plots the power curves :
                - Consumption form the dwelling load, labelled 'dwelling
                consumption'
                - grid_productions A and B, labelled 'grid_production A' and
                'grid_production B'
    """

    if LpStatus[model.status] == 'Optimal':
        print("\n - - - - - OPTIMIZATION RESULTS - - - - - ")
        print('Dwelling consumption = {0} kWh.'.format(
            dwelling_consumption.e_tot))
        print('grid_production A production = {0} kWh'.format(
            grid_production_A.e_tot))
        print('grid_production B production = {0} kWh'.format(
            grid_production_B.e_tot))

        # Plot the figures
        fig1 = plt.figure(1)
        ax1 = plt.axes()
        legend1 = []

        plot_quantity(time, dwelling_consumption.p, fig1, ax1)
        legend1 += ['Dwelling consumption']

        plt.legend(legend1)

        fig2 = plt.figure(2)
        ax2 = plt.axes()
        legend2 = []

        plot_quantity(time, grid_production_A.p, fig2, ax2)
        legend2 += ['grid production A']

        plot_quantity(time, grid_production_B.p, fig2, ax2)
        legend2 += ['grid production B']

        plt.legend(legend2)

        plt.show()

    elif LpStatus[model.status] == 'Infeasible':
        print("Sorry, the optimisation problem has no feasible solution !")

    elif LpStatus[model.status] == 'Unbounded':
        print("The cost function of the optimisation problem is unbounded !")

    elif LpStatus[model.status] == 'Undefined':
        print("Sorry, a feasible solution has not been found (but may exist). "
              "PuLP does not manage to interpret the solver's output, "
              "the infeasibility of the MILP problem may have been "
              "detected during presolve")

    else:
        print("Sorry, the optimisation problem has not been solved.")


def dashboard(production_optim, print_results_optimization):
    def runplot(click):
        clear_output()
        print_results_optimization()
        display(form)
        form.children[len(form_items) - 1].on_click(updateData)

    def updateData(click):
        clear_output()
        i = 1

        CONSUMPTION_PROFILE = form.children[i].children[1].value;
        i += 1

        OPERATING_COSTS_A_str = form.children[i].children[1].value.split(",")
        str = form.children[i].children[1].value
        OPERATING_COSTS_A = [float(s) for s in OPERATING_COSTS_A_str];
        i += 1

        OPERATING_COSTS_B_str = form.children[i].children[1].value.split(",")
        str = form.children[i].children[1].value
        OPERATING_COSTS_B = [float(s) for s in OPERATING_COSTS_B_str];
        i += 1

        MODEL, TIME, DWELLING_CONSUMPTION, GRID_PRODUCTION_A, \
        GRID_PRODUCTION_B = production_optim(op_cost_a=OPERATING_COSTS_A,
                                             op_cost_b=OPERATING_COSTS_B,
                                             consumption_file=CONSUMPTION_PROFILE)

        display(runPlotButton)
        runPlotButton.on_click(runplot)

    form_item_layout = Layout(
        display='flex',
        flex_flow='row',
        justify_content='space-between',
        width='100%'
    )

    form_items = [
        Box([HTML(value=('Here you update the inputs. Then, press '
                         '<b>Update</b> to run the optimisation.\
                 If the there is a solution you will be able to <b>Plot '
                         'results</b>.\
                  In case of no solution, it will show a warning.'),
                  placeholder='', description='')],
            layout=Layout(width='auto', grid_area='header')),
        Box([Label(
            value='File with hourly data for the consumption profile during '
                  'the day'),
            Textarea(placeholder='./data/Building_consumption_day.txt',
                     value='./data/Building_consumption_day.txt')],
            layout=form_item_layout, description_width='initial'),
        Box([Label(
            value='Hourly operating costs for the production unit A (0h '
                  'to 23h) separated by ","'),
             Textarea(placeholder='00h, 01h, 02h, 03h, 04h, ..., 23h',
                      value='41.1, 41.295, 43.125, 51.96, 58.275, 62.955, '
                            '58.08,\
 57.705, 59.94, 52.8, 53.865, 46.545, 41.4, 39,\
 36.87, 36.6, 39.15, 43.71, 45.195, 47.04, 44.28,\
 39.975, 34.815, 28.38', layout=Layout(width='40%', height='100px'))],
            layout=form_item_layout),
        Box([Label(
            value='Hourly operating costs for the production unit B (0h '
                  'to 23h) separated by ","'),
             Textarea(placeholder='00h, 01h, 02h, 03h, 04h, ..., 23h',
                      value='58.82, 58.23, 51.95, 47.27, 45.49, 44.5, 44.5,\
 44.72, 44.22, 42.06, 45.7, 47.91, 49.57, 48.69,\
 46.91, 46.51, 46.52, 51.59, 59.07, 62.1, 56.26, 55,\
 56.02, 52', layout=Layout(width='40%', height='100px'))],
            layout=form_item_layout),
        Button(
            description='Update',
            disabled=False,
            button_style='info',  # 'success', 'info', 'warning', 'danger' or ''
            tooltip='Click me',
            icon='check')

    ]

    form = Box(form_items, layout=Layout(
        display='flex',
        flex_flow='column',
        border='solid 2px',
        width='100%'
    ))

    runPlotButton = widgets.Button(
        description='Plot',
        disabled=False,
        button_style='success',  # 'success', 'info', 'warning', 'danger' or ''
        tooltip='Click me',
        icon='check')

    display(form)
    form.children[len(form_items) - 1].on_click(updateData)
